package com.twuc.webApp.yourTurn;

import com.twuc.webApp.bean.DependentBean;
import com.twuc.webApp.bean.WithoutDependencyBean;
import com.twuc.webApp.interfacee.Interface;
import com.twuc.webApp.otherbean.OutOfScanningScope;
import com.twuc.webApp.simple.SimpleDependent;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicIOCTest {

    //2,1,1
    @Test
    void should_return_without_dependency_bean() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.bean");
        assertNotNull(context.getBean(WithoutDependencyBean.class));
    }

    //2,1,2
    @Test
    void should_return_without_dependency_bean_and_dependent_bean() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.bean");
        assertNotNull(context.getBean(DependentBean.class));
        assertNotNull(context.getBean(WithoutDependencyBean.class));
    }

    /*
    //2,1,3  报错
    @Test
    void should_not_return_out_of_scanning_scope() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.bean");
        assertNull(context.getBean(OutOfScanningScope.class));
    }

     */

    //2,1,4 可以获取到
    @Test
    void should_return_interface_impl() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.interfacee");
        assertNotNull(context.getBean(Interface.class));
    }


    //2,1,5
    @Test
    void should_return_bean_name_O_o() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.WebApp.simple");
        System.out.println(context.getBean(SimpleDependent.class).getName());
        assertEquals(context.getBean(SimpleDependent.class).getName(), "O_o");
    }



    //证明bean singleton
    @Test
    void should_equlas_two_bean() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.WebApp.bean");
        assertNotEquals(context.getBean(WithoutDependencyBean.class), context.getBean(WithoutDependencyBean.class));
    }
}
