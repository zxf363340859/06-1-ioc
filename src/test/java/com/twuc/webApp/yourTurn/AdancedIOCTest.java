package com.twuc.webApp.yourTurn;

import com.twuc.webApp.bean.MultipleConstructor;
import com.twuc.webApp.bean.WithAutowiredMethod;
import com.twuc.webApp.implement.InterfaceWithMultipleImpls;
import com.twuc.webApp.interfacee.InterfaceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AdancedIOCTest {

    @Test
    void should_use_first_constructor() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.bean");
        List<String> logger = context.getBean(MultipleConstructor.class).getLogger();
        assertEquals("MultipleConstructor:dependentBean", logger.get(0));
    }

    // 构造器和普通方法都被使用 , 先调用构造函数，后调用普通方法,都不为空
    @Test
    void test_construct_and_method_transfer() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.bean");
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        assertEquals(2, withAutowiredMethod.getLogger().size());
        assertEquals("DependentBean", withAutowiredMethod.getLogger().get(0));
        assertNotNull(withAutowiredMethod.getAnotherDependent());
        assertNotNull(withAutowiredMethod.getDependentBean());
    }

    @Test
    void test_array_include_one_object() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.implement");
        Map<String, InterfaceWithMultipleImpls> beansOfType = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        System.out.println(beansOfType.toString());
        InterfaceWithMultipleImpls[] interfaceWithMultipleImplss = new InterfaceWithMultipleImpls[3];
        List<InterfaceWithMultipleImpls> list = new ArrayList<>();
        Set<String> strings = beansOfType.keySet();
        for (String s: strings) {
            list.add(beansOfType.get(s));
        }
        list.toArray(interfaceWithMultipleImplss);
        assertEquals(3, interfaceWithMultipleImplss.length);
    }
}
