package com.twuc.webApp.simple;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

public class SimpleDependent {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
