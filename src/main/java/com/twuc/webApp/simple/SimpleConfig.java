package com.twuc.webApp.simple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleConfig {

    @Bean
    public SimpleDependent getSimpleDependent() {
        SimpleDependent dependent = new SimpleDependent();
        dependent.setName("O_o");
        return dependent;
    }

}
