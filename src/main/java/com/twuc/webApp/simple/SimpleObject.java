package com.twuc.webApp.simple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObject implements SimpleInterface {

    private SimpleDependent simpleDependent;

    public SimpleObject(SimpleDependent simpleDependent) {
        simpleDependent = simpleDependent;
    }
}

