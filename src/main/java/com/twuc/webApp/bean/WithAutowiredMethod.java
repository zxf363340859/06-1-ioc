package com.twuc.webApp.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WithAutowiredMethod {

    private DependentBean dependentBean;
    private AnotherDependent anotherDependent;
    private List<String> logger = new ArrayList<>();

    public WithAutowiredMethod(DependentBean dependentBean) {
        logger.add("DependentBean");
        this.dependentBean = dependentBean;
    }

    @Autowired
    public void initialize (AnotherDependent anotherDependent) {
        logger.add("AnotherDependent");
        this.anotherDependent = anotherDependent;
    }

    public List<String> getLogger() {
        return logger;
    }

    public DependentBean getDependentBean() {
        return dependentBean;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }
}
