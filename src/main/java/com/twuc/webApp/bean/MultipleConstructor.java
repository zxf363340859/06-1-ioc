package com.twuc.webApp.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MultipleConstructor {

    private DependentBean dependentBean;
    private String string;
    private List<String> logger = new ArrayList<>();;

    public MultipleConstructor() {
    }

    @Autowired
    public MultipleConstructor(DependentBean dependentBean) {
        super();
        logger.add("MultipleConstructor:dependentBean");
        this.dependentBean = dependentBean;
    }

    public MultipleConstructor(String string) {
        super();
        logger.add("MultipleConstructor:string");
        this.string = string;
    }

    public List<String> getLogger() {
        return logger;
    }
}
